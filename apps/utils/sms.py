import threading

from django.conf import settings
# from zeep import Client
from xml.etree import ElementTree as et
from enum import Enum
import os
from techout.melipayamak import Api


def send_sms(phone, message):
    username = settings.MELIPAYAMAK_USERNAME
    password = settings.MELIPAYAMAK_PASSWORD
    api = Api(username, password)
    sms = api.sms()
    to = str(phone)
    _from = settings.MELIPAYAMAK_FROM
    text = message
    response = sms.send(to, _from, text)
    return response['RetStatus']


def send_authenticate_sms(phone, data):
    username = settings.MELIPAYAMAK_USERNAME
    password = settings.MELIPAYAMAK_PASSWORD
    bodyId = settings.MELIPAYAMAK_AUTHENTICATE_CODE
    api = Api(username, password)
    sms = api.sms()
    to = str(phone)
    response = sms.send_by_base_number(data, to, bodyId)
    return response['RetStatus']


# error codes
# class MessageStatus(Enum):
#     NOT_IN_ERROR_LIST = 1
#     WAIT_FOR_SENT = 40
#     SENT = 41
#     DIDNOT_SENT = 42
#     EXPIRED = 43
#     SENDING = 44
#     CANCELED = 45
#     BLACK_LIST = 46
#     ERROR = 47
#     DELIVERD = 48
#     WATING_FOR_DELIVER = 49
#     DELETED = 52
#     WAIT_FOR_ACCEPT = 53
#     LACK_OF_CREDIT = 55
#     READY_FOR_SEND = 57
#     PREPARING = 58


# this function is for parse base sendRequest xml and replace mobile_number
# and message to proper tags
# inputs:
#   mobile_number: string
#   message: string
# output:
#   return string of edited xml(sendRequest.xml)
# def parse_xml(mobile_number, message):
#     xml_address = os.path.join(settings.BASE_DIR, 'sendRequest.xml')
#     tree = et.parse(xml_address)
#     recipient_tag = tree.getroot().find('./body/recipient')
#     recipient_tag.text = '<![CDATA[' + message + ']]>'
#     recipient_tag.attrib["mobile"] = mobile_number
#     return et.tostring(tree.getroot())


# this function is for send sms to specific mobile_number and message
# inputs:
#   mobile_number: string
#   message: string
# output:
#   return string of response of webservice
# def send_sms(mobile_number, message):
#     SendSmsService(mobile_number, message).start()
#
#
# class SendSmsService(threading.Thread):
#     def __init__(self, mobile_number, message):
#         threading.Thread.__init__(self)
#         self.mobile_number = mobile_number
#         self.message = message
#
#     def run(self):
#         xml_request = parse_xml(self.mobile_number, self.message)
#         wsdl_address = os.path.join(settings.BASE_DIR, 'sms.wsdl')
#         client = Client(wsdl=wsdl_address)
#         try:
#             response = client.service.XmsRequest(xml_request)
#         except Exception as e:
#             print(e)
#             response = "Connection Error"
#             return response
#         tree = et.ElementTree(et.fromstring(response))
#         # this is not used maybe used later
#         response_message = tree.getroot().find('./code').text
#         # this is not used maybe used later
#         message_id = tree.getroot().find('./code').attrib["id"]
#         # this is not used maybe used later
#         try:
#             message_sent_status = MessageStatus(int(tree.getroot().find('./body/recipient').attrib["status"]))
#         except ValueError:
#             message_sent_status = MessageStatus(MessageStatus.NOT_IN_ERROR_LIST)
#         return message_sent_status

# test
# print(send_sms('09124335191', 'This Message Sent From Python, The Job Is Done'))
