from datetime import datetime, timedelta

from django.core.management.base import BaseCommand

from apps.crawler.handlers import crawler
from apps.crawler.models import Config


class Command(BaseCommand):
    help = 'Start Crawling'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        crawler()
