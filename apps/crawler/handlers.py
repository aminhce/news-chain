import feedparser
from datetime import datetime
from django.utils.timezone import make_aware

from .models import News, Agency


def agency_crawler(agency):
    feed = feedparser.parse(agency.rss)
    title_tag = str(agency.title_tag)
    link_tag = str(agency.link_tag)
    pub_date_tag = str(agency.pub_date_tag)

    for post in feed.entries:
        date_str = post[pub_date_tag]
        date = make_aware(datetime.strptime(date_str, '%a, %d %b %Y %H:%M:%S %Z'))
        News.objects.get_or_create(title=post[title_tag],
                                   agency=agency,
                                   link=post[link_tag],
                                   pub_date=date)


def crawler():
    agencies = Agency.objects.all()
    for agency in agencies:
        agency_crawler(agency)
