from django.contrib import admin
from .models import Agency, News, Config


@admin.register(Agency)
class AgencyAdmin(admin.ModelAdmin):
    save_as = True


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    save_as = True


@admin.register(Config)
class ConfigAdmin(admin.ModelAdmin):
    save_as = True
