# Generated by Django 3.1.3 on 2020-11-20 15:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Agency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True, verbose_name='date of create')),
                ('modify_on', models.DateTimeField(auto_now=True, null=True, verbose_name='date of modify')),
                ('name', models.CharField(blank=True, max_length=200, null=True)),
                ('website', models.URLField(blank=True, null=True)),
                ('rss', models.URLField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True, verbose_name='date of create')),
                ('modify_on', models.DateTimeField(auto_now=True, null=True, verbose_name='date of modify')),
                ('title', models.CharField(blank=True, max_length=300, null=True)),
                ('link', models.URLField()),
                ('pub_date', models.DateTimeField(blank=True, null=True)),
                ('available', models.BooleanField(default=True)),
                ('agency', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='crawler.agency')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
