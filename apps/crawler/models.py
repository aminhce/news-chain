from django.contrib.auth.models import User
from django.db import models
from apps.utils.models import LoggableModel


class Agency(LoggableModel):
    name = models.CharField(max_length=200, blank=True, null=True)
    website = models.URLField(blank=True, null=True)
    rss = models.URLField()

    title_tag = models.CharField(max_length=100, blank=True, null=True)
    # agency_tag = models.CharField(max_length=100, blank=True, null=True)
    link_tag = models.CharField(max_length=100, blank=True, null=True)
    pub_date_tag = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name


class News(LoggableModel):
    title = models.CharField(max_length=300, blank=True, null=True)
    agency = models.ForeignKey(Agency, on_delete=models.CASCADE)
    link = models.URLField()
    pub_date = models.DateTimeField(blank=True, null=True)
    available = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class Config(models.Model):
    username = models.OneToOneField(User, on_delete=models.CASCADE)
    start_crawling = models.TimeField(blank=True, null=True)
    frequency = models.DurationField(blank=True, null=True)

    def __str__(self):
        return str(self.username.get_full_name()) + ' ' + str(self.start_crawling)




